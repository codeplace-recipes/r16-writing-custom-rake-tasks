
namespace :contest do
  desc 'Welcome the contestants'
  task :hello do
    puts 'Hello World!'
  end

  desc 'Full greeting'
  task ask: :hello do
    puts 'How are you?'
  end

  desc 'Pick a prize'
  task prize: :environment do
    puts "Prize: #{pick(Product).title}"
  end

  desc 'Pick a winner'
  task winner: :environment do
    puts "Winner: #{pick(User).name}"
  end

  desc 'Run a contest (all tasks)'
  task run: [:ask, :prize, :winner]

  def pick(model_class)
    model_class.order('RANDOM()').first
  end
end
