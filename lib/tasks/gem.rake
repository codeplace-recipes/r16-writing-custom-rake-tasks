namespace :gem do
  desc 'Add a gem to the Gemfile'
  task :add, [:name] => :environment do |_task, args|
    sh %(echo "gem '#{args.name}'" >> Gemfile)
    sh %(bundle install)
    sh %(git add --all)
    sh %(git commit -m "Add gem #{args.name} to app")
  end

  desc 'Remove the last gem from the Gemfile'
  task remove: :environment do
    sh %(head -n -1 Gemfile > Gemfile.tmp ; mv Gemfile.tmp Gemfile)
    sh %(bundle install)
    sh %(git add --all)
    sh %(git commit -m "Remove the last gem")
  end
end
